import { useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './Components/Header';
import NavListData from './data/NavListData';
import "bootstrap/dist/css/bootstrap.min.css"
import "./app.css";
import Home from './Pages/Home';
import { AuthProvider } from './Authentication/AuthContext';

function App() {

  // The scroll position of window
  const [scroll, setScroll] = useState(0);

  const [searchTerm, setSearchTerm] = useState("");
  const [searchEndPoint, setSearchEndpoint] = useState("");

  // Constantly listen to the scroll position
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY);
    });
    return () => {
      window.removeEventListener("scroll", () => {
        setScroll(window.scrollY);
      });
    };
  }, [scroll]);
  
  return (
    <Router>
      <AuthProvider>
        <div className='container-fluid movie-app'>
          <Header setSearchTerm={setSearchTerm} searchTerm={searchTerm} setSearchEndpoint={setSearchEndpoint} scroll={scroll}/>
            <Routes>
              <Route path="/" element={<Home endpoint={searchEndPoint} />} />
              {NavListData.map(({ link, id, component: Component }) => (
                <Route key={id} path={link} element={<Component />} />
              ))}
            </Routes>       
        </div>  
      </AuthProvider>
    </Router>
  )
}

export default App
