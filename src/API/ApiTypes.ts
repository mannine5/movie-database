

export interface Movie {
  adult: boolean;
  backdrop_path: string | null;
  genre_ids: number[];
  id: number | string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string | null;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

/*
export interface ApiResponse {
  page?: number; // Make page optional
  results: Movie[] | Movie; // Results can be either a single movie or an array of movies
  total_pages?: number; // Make total_pages optional
  total_results?: number; // Make total_results optional
}
*/