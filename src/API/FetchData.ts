import axios from "axios";
import { Movie } from "./ApiTypes";

interface FetchDataProps {
  endpoint: string;
  setMovieData: (update: (movieData: Movie[]) => Movie[]) => void;
  setError: (error: string | null) => void;
}

export default function FetchData({endpoint, setMovieData, setError}: FetchDataProps, page: number = 1) {
  
  if (endpoint === "http://localhost:3001/FavouritesDatabase/") {

    axios.get(endpoint)
      .then((response) => {
      
      console.log("These are Favourite database movies: ")
    
      if (response.data) {
    
        response.data.forEach((movie: Movie) => {
          console.log(movie.id); 
          console.log(movie.title);
        });

        setMovieData(movieData => {
          
          const updatedMovieData = [...movieData, ...response.data];

          console.log("Banner state updated with the database movies:")  
          
          updatedMovieData.forEach((movie: Movie) => {
            console.log(movie.id + " " + movie.title); 
            
          })

          return updatedMovieData;
          
        });

      } else {
        setError("No results found.");
      }

    })  
    .catch ((error) => {
      console.error("Error fetching data:", error);
      setError("Error fetching data.");
    })
  } 
  
  else {

    axios.get(endpoint + `${page}`, {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${import.meta.env.VITE_API_READ_ACCESS_TOKEN}`
      }
    })
    .then((response) => {
      
      console.log("This is page: " + page)
    
      if (response.data.results.length !== 0) {
    
        response.data.results.forEach((movie: Movie) => {
          console.log(movie.id); 
          console.log(movie.title);
        });

        setMovieData(movieData => {
          
          // Extract existing movie IDs from movieData
          const existingMovieIds = movieData.map(movie => movie.id);

          // Filter response data to remove movies with existing IDs
          const filteredResponseData = response.data.results.filter((movie: Movie) => !existingMovieIds.includes(movie.id));

          // Concatenate filtered response data with existing movie data
          const updatedMovieData = [...movieData, ...filteredResponseData];

          console.log(updatedMovieData);

          return updatedMovieData;
          
        });

      } else {
        setError("No results found.");
      }

      if (response.data.page < 5 && response.data.page < response.data.total_pages) {
        FetchData({ endpoint, setMovieData, setError }, page + 1);
      }

    })  
    .catch ((error) => {
      console.error("Error fetching data:", error);
      setError("Error fetching data.");
    })
  }  
}

