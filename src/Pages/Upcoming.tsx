import Banner from "../Components/Banner";
import AddFavourites from "../Components/AddFavourites";

const upcomingEndPoint = "https://api.themoviedb.org/3/movie/upcoming?language=en-US&page="

export default function Upcoming() {

  return (
    <div style={{marginTop:100}}>
      <p>Here'll be Upcoming!</p>
      <Banner endpoint={upcomingEndPoint} overlayComponent={(movie) => <AddFavourites movie={movie} />}/>
    </div>
  )
}