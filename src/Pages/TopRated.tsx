import Banner from "../Components/Banner"
import AddFavourites from "../Components/AddFavourites";

// Define the endpoint for Top Rated movies
const topRatedEndpoint = "https://api.themoviedb.org/3/movie/top_rated?language=en-US&page=";

export default function TopRated() {

  

  return (
    
    <div style={{marginTop:100}}>
      <p>Here'll be Top Rated!</p>
      <Banner endpoint={topRatedEndpoint} overlayComponent={(movie) => <AddFavourites movie={movie} />}/>
    </div>
  )
}