import Banner from "../Components/Banner";
import AddFavourites from "../Components/AddFavourites";

interface HomeProps {
  endpoint: string
}

export default function Home(props: HomeProps) {
  

  return (
    <div style={{marginTop:100}}>
      <p>This'll be the page for Home and Search!</p>
      <Banner endpoint={props.endpoint} overlayComponent={(movie) => <AddFavourites movie={movie} />}/>
    </div>
  )
}