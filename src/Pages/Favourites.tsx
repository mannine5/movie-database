import Banner from "../Components/Banner"
import RemoveFavourites from "../Components/RemoveFavourites"
import { Movie } from "../API/ApiTypes";


// Define the endpoint for Favourite movies
const favouriteEndPoint = "http://localhost:3001/FavouritesDatabase/"

export default function Favourites() {

  return (
    <div style={{marginTop:100}}>
      <p> Here'll be Favourites! </p>
      <Banner
        endpoint={favouriteEndPoint}
        overlayComponent={(
          movie: Movie, 
          setMovieData: (movies: Movie[]) => void
          ) => (
            <RemoveFavourites movie={movie} setMovieData={setMovieData} />
        )}
      />
    </div>
  )
}