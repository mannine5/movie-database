import Banner from "../Components/Banner"
import AddFavourites from "../Components/AddFavourites"

// Define the endpoint for Popular movies
const popularEndPoint = "https://api.themoviedb.org/3/movie/popular?language=en-US&page="

export default function Popular() {
  

  return (
    <div style={{marginTop:100}}>
      <p>Here'll be Populars!</p>
      <Banner endpoint={popularEndPoint} overlayComponent={(movie) => <AddFavourites movie={movie} />}/>
    </div>
  )
}