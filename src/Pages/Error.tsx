interface ErrorProps {
  errorMessage: string | null;
}

export default function Error({ errorMessage }: ErrorProps) {
  
  return (
    <div>
      <div className="alert alert-danger" role="alert">
          {errorMessage}
        </div>
    </div>
  )
}