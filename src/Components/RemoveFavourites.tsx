import axios from "axios"
import { Movie } from "../API/ApiTypes"

interface AddFavouritesProps {
  movie: Movie;
  setMovieData: (movies: Movie[]) => void;
}

export default function RemoveFavourites({ movie, setMovieData }: AddFavouritesProps) {

  function handleClick() {
    
    axios.delete(`http://localhost:3001/FavouritesDatabase/${movie.id}`)
      .then(response => {

        console.log('Movie removed from favourites:', response.data);

        axios.get(`http://localhost:3001/FavouritesDatabase/`)
          .then((response) => {
            
            console.log("These are Favourite database movies: ")
        
            if (response.data) {
        
              response.data.forEach((movie: Movie) => {
                console.log(movie.id); 
                console.log(movie.title);
              });
      
              setMovieData(response.data)
              
              console.log("Banner state updated with the database movies:")  
              
              response.data.forEach((movie: Movie) => {
                console.log(movie.id + " " + movie.title);  
              })
            }
          })  
      })
    .catch(error => {
      console.error('Error removing movie from favourites:', error);
    });    
  }
  
  return (
    <>
      <span className="mr-2" onClick={handleClick}> 
        Remove from Favourites <i className="bi bi-x-square"></i>
      </span>
    </>
  )
}