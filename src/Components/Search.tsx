import { useNavigate } from "react-router-dom";
import "./CSS/search.css"

interface HeaderProps {
  searchTerm: string
  setSearchTerm: (term: string) => void;
  setSearchEndpoint: (term:string) => void;
}

export default function Search(props: HeaderProps) {

  const navigate = useNavigate();

  function handleFormSubmit (event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (props.searchTerm.trim() !== '') {
      console.log('Searching for:', props.searchTerm);
      props.setSearchEndpoint(`https://api.themoviedb.org/3/search/multi?query=${props.searchTerm}&include_adult=false&language=en-US&page=`)
      navigate("/");
    }
  }

  return ( 
    <div className="search">
      <form onSubmit={handleFormSubmit}>
        <input 
          type="text"
          name="searchField" 
          value={props.searchTerm} 
          onChange={(event) => props.setSearchTerm(event.target.value)} 
          placeholder="Search" />
        <button>
          <i className="bi-search"></i>
        </button>
      </form>
    </div> 
  )

}