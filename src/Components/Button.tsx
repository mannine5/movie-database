import "./CSS/button.css"

interface ButtonProps{
  name: string;
  bgColor?: string; // Make bgColor optional
  color?: string; // Make color optional
  icon: JSX.Element;
  handleClick: () => void
}

export default function Button(props: ButtonProps) {

  // Apply default values if props are not provided
  const buttonBgColor = props.bgColor || "#ff3700";
  const buttonColor = props.color || "#ffffff";

  return (
    <button className="mainBtn" style={{color: buttonColor, background: buttonBgColor}} onClick={props.handleClick}>
      {props.icon} {props.name}
    </button>
  )
}