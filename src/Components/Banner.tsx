import FetchData from "../API/FetchData";
import { useState, useEffect, useContext } from "react";
import { Movie } from "../API/ApiTypes";
import "bootstrap/dist/css/bootstrap.min.css"
import Error from "../Pages/Error"
import "./CSS/banner.css"
import { AuthContext } from "../Authentication/AuthContext"

interface BannerProps {
  endpoint?: string; // Define a prop for endpoint
  overlayComponent: (
    movie: Movie, 
    setMovieData: (movies: Movie[]) => void
    ) => JSX.Element
}

export default function Banner({ endpoint, overlayComponent }: BannerProps) {

  const [movieData, setMovieData] = useState<Movie[]>([]);
  const [error, setError] = useState<string | null>(null); 
  const authContext = useContext(AuthContext);
  
  useEffect(() => {
    if (endpoint) {
      setMovieData([])
      FetchData({ endpoint, setMovieData: setMovieData, setError: setError });
    }
  }, [endpoint]);
 
  return (
    <div className="container">
      {error ? (
        <Error errorMessage={error} />
      ) : (
        <div className="row justify-content-center">
          {movieData.map((movie) => (
            <div key={movie.id} className="col-12 col-md-3 col-sm-4 d-flex mb-4" >
              {movie.poster_path ? (
                <div className="image-container d-flex justify-content-start">
                <img 
                  src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`} 
                  alt={movie.title} 
                  className="img-fluid" 
                />
                  {authContext?.isAuthenticated && ( // Render overlay only if authenticated
                    <div className="overlay d-flex align-items-center justify-content-center">
                      {overlayComponent(movie, setMovieData)}
                    </div>
                  )}
                </div>
              ) : (
                <div className="image-container align-items-center justify-content-start ">
                  <img
                    src="/Cat.jpg" // Path to your alternate image
                    alt={movie.title} 
                    className="img-fluid" 
                  />
                  <p>{movie.title ? movie.title : "Not name found."}</p>
                </div>
            )}
            </div>
          ))}
        </div>
      )}
    </div>
  );
}