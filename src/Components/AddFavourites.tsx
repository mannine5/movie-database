import axios from "axios"
import { Movie } from "../API/ApiTypes"


interface AddFavouritesProps {
  movie: Movie;
  setMovieData?: (movies: Movie[]) => void;
}

export default function AddFavourites({ movie }: AddFavouritesProps) {


  function handleClick() {

    const movieToAdd: Movie = movie

    // Convert the id to string if it's a number
    if (typeof movieToAdd.id === 'number') {
      movieToAdd.id = movieToAdd.id.toString();
    }
    
    axios.get(`http://localhost:3001/FavouritesDatabase/`)
      .then(response => {
        // If the movie already exists, log a message and don't add it again
        const isDuplicate = response.data.some((existingMovie: Movie) => existingMovie.id === movieToAdd.id);
        
        if (isDuplicate) {
          console.log('Movie already exists in the database.');
        } else {
          // If the movie doesn't exist, add it to the database
          axios.post(`http://localhost:3001/FavouritesDatabase/`, movieToAdd)
            .then(response => {
              console.log('New movie added:', response.data);
            })
            .catch(error => {
              console.error('Error adding new movie:', error);
            });
        }
      })
      .catch(error => {
        console.error('Error checking for existing movie:', error);
      });
  }


  return (
    <>
      <span className="mr-2" onClick={handleClick}> 
        Add to Favourites <i className="bi bi-heart-fill" style={{color: "red"}}/> 
      </span>
    </>
  )

}