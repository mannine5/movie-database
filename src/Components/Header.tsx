import NavListItem from "./NavListItem"
import NavListData from "../data/NavListData"
import Search from "./Search"
import "./CSS/header.css"
import Button from "./Button"
import { useContext } from "react"
import { AuthContext } from "../Authentication/AuthContext"
import { Link } from "react-router-dom"

interface HeaderProps {
  searchTerm: string
  setSearchTerm: (term: string) => void;
  setSearchEndpoint: (term:string) => void;
  scroll: number
}

interface AuthContextType {
  isAuthenticated: boolean;
  signIn: () => void;
  signOut: () => void;
}

export default function Header(props: HeaderProps) {

  const { isAuthenticated, signIn, signOut } = useContext(AuthContext) as AuthContextType;

  function handleSignIn() {
    signIn(); // Call signIn function from context
  }

  function handleSignOut() {
    signOut(); // Call signOut function from context
  }

  let navList = [...NavListData];

  if (!isAuthenticated) {
    navList = navList.filter(item => item.id !== 4); // Filter out Favourites if not authenticated
  }

  return (
    <header className={`${props.scroll > 100 ? "scrolled" : undefined}`}>
      <a href="/" className="logo">Movie Database</a>
      <nav>
        <ul className="nav">
          {navList.map((item) => 
          <NavListItem item={item} key={item.id}/>)}
        </ul>
      </nav>
      <Search 
        setSearchTerm={props.setSearchTerm} 
        searchTerm={props.searchTerm} 
        setSearchEndpoint={props.setSearchEndpoint}
      />
      {isAuthenticated ? (
        <Link to="/">
          <Button
          icon={<i className='bi bi-box-arrow-in-right'></i>}
          name='Sign Out'
          bgColor="#ffffff" 
          color="#ff3700" 
          handleClick={handleSignOut}
          />
        </Link>
      ) : (
        <Button
          icon={<i className="bi bi-box-arrow-in-right"></i>}
          name="Sign In"
          handleClick={handleSignIn} />
      )}
    </header>
  )
}