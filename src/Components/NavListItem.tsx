import { Link } from 'react-router-dom'
import "./CSS/navListItem.css"

type NavItem = {
  item: {
    id: number;
    link: string;
    name: string;
    active: boolean;
  }
}

export default function NavListItem({item} : NavItem) {
  return (
    <li>
     <Link className='link' to={item.link}>{item.name} </Link>  
    </li>
  )
}