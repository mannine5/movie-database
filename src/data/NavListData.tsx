import Favourites from "../Pages/Favourites";
import Popular from "../Pages/Popular";
import TopRated from "../Pages/TopRated";
import Upcoming from "../Pages/Upcoming";
import { ComponentType } from 'react';

interface NavItem {
  id: number;
  link: string;
  name: string;
  active: boolean;
  component: ComponentType; 
}
const NavListData: NavItem[] = [
  {
    id: 1,
    link: "/Popular",
    name: "Popular",
    active: false,
    component: Popular
  },
  {
    id: 2,
    link: "/Top Rated",
    name: "Top Rated",
    active: false,
    component: TopRated
  },
  {
    id: 3,
    link: "/Upcoming",
    name: "Upcoming",
    active: false,
    component: Upcoming
  },
  {
    id: 4,
    link: "/Favourites",
    name: "Favourites",
    active: false,
    component: Favourites
  }
]

export default NavListData

// First I tried to make navBar "dynamically", but desided not to.
// It worked for some degree, but there was problems.


/*
import { ComponentType } from 'react';
import * as Pages from '../Pages/NotInUse(index)';

// Define the interface for a navigation item
interface NavItem {
  id: number;
  link: string;
  name: string;
  active: boolean;
  component: ComponentType;
}

// Define a function to dynamically import all page components from the Pages folder
const importAllPages = async (): Promise<{ [key: string]: ComponentType }> => {
  const pageNames = Object.keys(Pages);
  const importedPages: { [key: string]: ComponentType } = {};

  // Import each page component dynamically
  await Promise.all(pageNames.map(async (pageName: string) => {
    const pageModule = await import(`../Pages/${pageName}.tsx`);
    importedPages[pageName] = pageModule.default;
  }));

  return importedPages;
};

// Generate the navNavListData object based on the imported page components
const generateNavListData = async (): Promise<NavItem[]> => {
  const importedPages = await importAllPages();
  return Object.entries(importedPages).map(([name, component], index) => ({
    id: index + 1,
    link: `/${name}`, // Generate link based on page name
    name,
    component,
    active: false, // Assuming active state needs to be set
  }));
};


// Export the generated navigation configuration
const NavListData = await generateNavListData();

export default NavListData
*/


// I had "index.tsx" for that.

/*
export { default as Favourites } from './Favourites';
export { default as Popular } from './Popular';
export { default as TopRated } from './TopRated';
export { default as Upcoming } from './Upcoming';
export { default as Search } from './Home';
*/ 