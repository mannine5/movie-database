Movie Database | React App | Buutti Exercise

This was a small school project for FrontEnd coding.

I used React Routing for linking the pages. Then I made one component for fetching data from TMDB database. Search functionality also uses this same component.

After that, I made a database file for favorites. "Add and Remove movies from Favorites" functionality uses JSON server to access and modify the database.

Lastly, I made Sign In and Log Out functionality using local storage to save the logging status.

And navbar has some cool CSS.

![Alt Text](https://gitlab.com/mannine5/movie-database/-/raw/main/Movie%20Database/Screenshot%201.jpg?ref_type=heads)

![Alt Text](https://gitlab.com/mannine5/movie-database/-/raw/main/Movie%20Database/Screenshot%202.jpg?ref_type=heads)

![Alt Text](https://gitlab.com/mannine5/movie-database/-/raw/main/Movie%20Database/Screenshot%203.jpg?ref_type=heads)

![Alt Text](https://gitlab.com/mannine5/movie-database/-/raw/main/Movie%20Database/Screenshot%204.jpg?ref_type=heads)
